# DeepGeoStat

This is the repository for the DeepGeoStat project. DeepGeoStat uses Docker containers to run its services.
The project consists of two main components for which separate Docker containers are built:

  * The web application;
  * The API.

# Note
The docker files that end with .dev are meant for development and/or testing.
The non .dev files are meant for deployment since the webserver is run with a production server and not a development one.